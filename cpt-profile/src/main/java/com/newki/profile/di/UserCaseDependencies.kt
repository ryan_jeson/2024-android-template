package com.newki.profile.di

import com.newki.profile_api.repository.ArticleUserCase
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * @EntryPoint 它允许Hilt外部的代码访问Hilt容器中的依赖，即使在Hilt不能直接管理的类中也能实现依赖注入
 *
 * 常见场景为路由服务中注入，手动创建的类或者通过工厂创建的类手动注入
 */
@EntryPoint
@InstallIn(SingletonComponent::class)
interface UserCaseDependencies {
    fun articleUserCase(): ArticleUserCase
}