package com.android.basiclib.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PageInfo<T> implements Serializable {

    public String count;
    public String curPage;
    public String pageSize;
    public String countPage;
    @SerializedName(value="list", alternate="data")
    public List<T> list;

}
