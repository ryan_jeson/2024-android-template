package com.android.basiclib.bean;

public class ErrorBean {
    public int code;
    public String message;

    @Override
    public String toString() {
        return "ErrorBean{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

}
