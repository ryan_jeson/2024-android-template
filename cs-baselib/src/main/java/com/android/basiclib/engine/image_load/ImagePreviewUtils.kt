package com.android.basiclib.engine.image_load

import android.content.Context
import android.graphics.Color
import android.widget.ImageView
import com.lxj.xpopup.XPopup
import com.lxj.xpopup.core.ImageViewerPopupView
import com.lxj.xpopup.interfaces.OnImageViewerLongPressListener
import com.lxj.xpopup.util.SmartGlideImageLoader

/**
 * 图片的预览引擎类，目前基于XPopup实现
 */
object ImagePreviewUtils {

    /**
     * 单图片的预览
     */
    fun singleImagePreview(
        context: Context,
        img: ImageView,
        imageUrl: String,
        placeholderColor: Int = -1,
        isInfinite: Boolean = false,
        placeholderStroke: Int = -1,
        roundRadius: Int = 0,
        isShowSaveBtn: Boolean = false,
        bgColor: Int = Color.BLACK,
        longPressListener: OnImageViewerLongPressListener
    ) {
        XPopup.Builder(context)
            .asImageViewer(
                img,
                imageUrl,
                isInfinite,
                placeholderColor,
                placeholderStroke,
                roundRadius,
                isShowSaveBtn,
                bgColor,
                SmartGlideImageLoader(),
                longPressListener
            )
            .show()
    }

    /**
     * 多图的选择
     */
    fun multipleImagePreview(
        context: Context,
        img: ImageView,
        list: List<Any>,
        position: Int = 0,
        placeholderRes: Int = 0,
        isInfinite: Boolean = false,
        placeholderStroke: Int = -1,
        isShowSaveBtn: Boolean = false,
        roundRadius: Int = 0,
        bgColor: Int = Color.BLACK,
        updatebAction: (popupView: ImageViewerPopupView, position: Int) -> Unit,
        longPressListener: OnImageViewerLongPressListener
    ) {
        //Xpopup的弹窗
        XPopup.Builder(context).asImageViewer(
            img,
            position,
            list,
            isInfinite,
            placeholderRes != 0,
            placeholderRes,
            placeholderStroke,
            roundRadius,
            isShowSaveBtn,
            bgColor,
            updatebAction,
            SmartGlideImageLoader(),
            longPressListener
        ).show()
    }


}