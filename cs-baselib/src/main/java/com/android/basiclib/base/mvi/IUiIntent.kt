package com.android.basiclib.base.mvi

import androidx.annotation.Keep

/**
 * @author Newki
 */
//MVI 页面【事件】管理的基类
//在R8代码混淆压缩时必须保留被标记的类或方法，以防止代码出现因混淆而导致的崩溃。
@Keep
interface IUiIntent