package com.newki.profile_api.http

import com.android.basiclib.bean.BaseBean
import com.android.cs_service.Constants
import com.newki.profile_api.entity.Banner
import com.newki.profile_api.entity.TopArticleBean

import retrofit2.http.GET


interface ProfileApiService {

    /**
     * 获取首页Banner
     */
    @GET(Constants.BANNER_URL)
    suspend fun fetchBanner(): BaseBean<List<Banner>>

    /**
     * 获取置顶文章
     */
    @GET(Constants.TOP_ARTICLE_URL)
    suspend fun fetchTopArticle(): BaseBean<List<TopArticleBean>>

}