package com.runalone.auth.app

import com.android.basiclib.base.BaseApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RunaloneApplication :BaseApplication(){
    override fun onCreate() {
        super.onCreate()
    }

}