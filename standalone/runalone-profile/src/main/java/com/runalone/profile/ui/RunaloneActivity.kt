package com.runalone.profile.ui

import android.os.Bundle
import com.android.basiclib.base.activity.BaseVVDActivity
import com.android.basiclib.ext.click
import com.newki.profile.ui.ProfileActivity
import com.runalone.profile.databinding.ActivityRunaloneBinding
import com.runalone.profile.mvvm.RunaloneViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * 独立运行模块入口
 */
@AndroidEntryPoint
class RunaloneActivity : BaseVVDActivity<RunaloneViewModel, ActivityRunaloneBinding>() {

    override fun init(savedInstanceState: Bundle?) {

        mBinding.btnLogin.click {
            ProfileActivity.startInstance()
            finish()
        }

    }

}