package com.newki.template.ui

import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import com.android.basiclib.base.activity.BaseVVDActivity
import com.android.basiclib.base.vm.EmptyViewModel
import com.android.basiclib.engine.toast.toast
import com.android.basiclib.ext.click
import com.android.cs_service.ARouterPath
import com.newki.app_api.router.AppServiceProvider
import com.newki.template.databinding.ActivityMainBinding

@Route(path = ARouterPath.PATH_PAGE_MAIN)
class MainActivity : BaseVVDActivity<EmptyViewModel, ActivityMainBinding>() {

    override fun init(savedInstanceState: Bundle?) {

        mBinding.btnLogin.click {
            ARouter.getInstance().build(ARouterPath.PATH_PAGE_AUTH_LOGIN).navigation()
        }

        mBinding.btnPushId.click {
            val pushId = AppServiceProvider.appService?.getPushTokenId()
            toast("pushId:$pushId")
        }

        mBinding.btnNews.click {
            NewsActivity.startInstance()
        }

    }

}
